# CHANGELOG

All notable changes to this project are documented in this file. The format aligns with principles
described via [Keep a CHANGELOG][change].

This project adheres to [Semantic Versioning][semver].

## Planned

### Added

No changes are presently planned.

Candidates for change are captured as [issues].

## [0.1.0] - 2017-07-25

### Added

- initial draft of utility
- project metadata via README, CHANGELOG, package.json, .gitignore
- eslint configuration

## [0.0.0] - 2017-07-25

Project initiated.

[change]: http://keepachangelog.com/
[semver]: http://semver.org/
[issues]: https://bitbucket.org/rhbecker/util-get-redacted-object-copy/issues?status=on+hold&status=open&status=new
[Unreleased]: https://bitbucket.org/rhbecker/util-get-redacted-object-copy/branches/compare/develop..master#diff
[0.1.0]: https://bitbucket.org/rhbecker/util-get-redacted-object-copy/branches/compare/v0.1.0%0Dv0.0.0#diff
[0.0.0]: https://bitbucket.org/rhbecker/util-get-redacted-object-copy/src/b7b2c79dee0a64d31d880d66e9b7c4cf514194b3/?at=v0.0.0
