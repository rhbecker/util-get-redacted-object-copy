# Utility: Get Redacted Object Copy

Returns a copy of a submitted object, minus the properties named in a submitted array. Think of the
returned object as a "redacted" edition of the original.

## Example

```JavaScript
const redact = require('@rhbecker/util-get-redacted-object-copy');

return redact({ a: 1, b: 2, c: 3 }, ['b']); // { a: 1, c: 3 }
```

## Syntax

```JavaScript
redact(original, shouldRedact)
```

### Parameters

* original (Object)

    An object for which a redacted copy should be returned.

* shouldRedact (Array)

    A list of names - one for each property in the original object that should not be found in the
    returned copy.

### Returns

(Object): A copy of the submitted object, minus the properties named in a submitted array.
