'use strict';

module.exports = (original, shouldRedact = []) => Object
  .keys(original)
  .filter(key => !shouldRedact.includes(key))
  .reduce((accumulator, key) => {
    const redacted = accumulator;
    redacted[key] = original[key];
    return redacted;
  }, {});
